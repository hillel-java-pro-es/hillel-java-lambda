package handler;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.Map;

public class Handler implements RequestHandler<Map<String, Object>, String> {

    @Override
    public String handleRequest(Map<String, Object> event, Context context) {
        var logger = context.getLogger();
        logger.log("EVENT TYPE: " + event);
        return "Hillel Java Pro 27.06.2023/20:51";
    }
}